#include <stdio.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include "md5.h"

char *md5(const char *str, int length)
{
    unsigned char *digest = md5digest(str, length);
    char *out = (char *)malloc(MD5_DIGEST_LENGTH * 2 + 1);
    
    for (int n = 0; n < MD5_DIGEST_LENGTH; ++n) {
        snprintf(&(out[n*2]), MD5_DIGEST_LENGTH*2, "%02x", (unsigned int)digest[n]);
    }

    out[MD5_DIGEST_LENGTH * 2] = '\0';
    
    return out;
}

    
unsigned char *md5digest(const char *str, int length) {
    MD5_CTX c;
    static unsigned char digest[MD5_DIGEST_LENGTH];
 
    MD5_Init(&c);

    while (length > 0) {
        if (length > 512) {
            MD5_Update(&c, str, 512);
        } else {
            MD5_Update(&c, str, length);
        }
        length -= 512;
        str += 512;
    }

    MD5_Final(digest, &c);

    return digest;
}

// Convert a hex digest (string) into an array of bytes
unsigned char *hex2digest(char *hex)
{
    static unsigned char digest[MD5_DIGEST_LENGTH];
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        int value;
        sscanf(hex + i * 2, "%02x", &value);
        digest[i] = value;
    }
    return digest;
}

// Convert an array of bytes into a hex digest (string)
char *digest2hex(unsigned char *digest)
{
    static char hex[MD5_DIGEST_LENGTH * 2 + 1];
    for (int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        snprintf(&(hex[i*2]), MD5_DIGEST_LENGTH*2, "%02x", (unsigned int)digest[i]);
    }
    hex[MD5_DIGEST_LENGTH * 2] = '\0';
    return hex;
}
