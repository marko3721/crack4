#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "entry.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


    //   For each hash (read one line at a time out of the file):
    //   Convert hex string to digest (use hex2digest. See md5.h)
    //   Extract the first three bytes
    //   Use those three bytes as a 24-bit number
int hex2dig(char *hash)
{
    unsigned char *dig;
    dig = hex2digest(hash);
    
    int idx = dig[0] * 65536 + dig[1] * 256 + dig[2];
    
    return idx;
}

    

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file rainbow_file\n", argv[0]);
        exit(1);
    }

    // Open the text hashes file for reading
    FILE *hf = fopen(argv[1], "r");
    if(hf == NULL)
    {
        printf("Error with opening hash file\n");
        exit(1);
    }

    // Open the binary rainbow file for reading
    FILE *rf = fopen(argv[2], "r");
    if(rf == NULL)
    {
        printf("Error with opening rainbow/password file\n");
        exit(1);
    }
    
    
    struct entry e;
    
    
    // Run while loop to complete task of cracking passwords
    char str[32];
    while(fscanf(hf,"%s\n", str) != EOF)
    {
        //   Extract the first three bytes
        //   Use those three bytes as a 16-bit number
        int idx = hex2dig(str);
        
        //   Seek to that location in the rainbow file
        //   Read the entry found there
        int file_location = idx * sizeof(struct entry);
        fseek(rf, file_location, SEEK_SET);
        fread(&e, sizeof(struct entry), 1, rf);
        int count = 1;
       
        //   Compare hashes, if not a match, then move to next spot
        while(memcmp(str, digest2hex(e.hash), 32) != 0)
        {
            
            fread(&e, sizeof(struct entry), 1, rf);
            
            // if it can not find after 10 tries
            count++;
            if (count ==10)
            break;
        }
        printf("%s = ", digest2hex(e.hash));
        printf("%s\n", e.pass); 
    }
    fclose(rf);
    fclose(hf);

}
